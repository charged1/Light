import discord

from discord.ext import commands
from discord.ui import Button, View

class Misc(commands.Cog, name="Misc"):
  def __init__(self, client):
      self.client = client

  @commands.command()
  async def help(self, ctx):
      embed = discord.Embed(
        title='Light Commands',
        description='Prefix: .',
        color=0x2ecc71
      )

      embed.add_field(
        name='💡 Misc',
        value='Commands that do not fit into the categories are here.'
      )

      embed.add_field(
        name='🎲 Fun',
        value='Have some fun with Light!'
      )

      button = Button(label="Misc", style=discord.ButtonStyle.green, emoji='💡')
      button2 = Button(label="Fun", style=discord.ButtonStyle.green, emoji='🎲')

      async def button_callback(interaction):
          button = Button(label="↤ Close Menu", style=discord.ButtonStyle.gray)

          embed = discord.Embed(
            title='FreeBot Commands',
            description='💡 **Misc**',
            color=0x2ecc71
          )

          embed.add_field(
            name='help',
            value='Displays the original help message.'
          )

          async def button_callback(interaction):
            embed = discord.Embed(description='Interaction Closed.')
            await interaction.response.edit_message(embed=embed, view=None)

          button.callback = button_callback

          view  = View()
          view.add_item(button)

          await interaction.response.edit_message(embed=embed, view=view)

      async def button2_callback(interaction):
          button = Button(label="↤ Close Menu", style=discord.ButtonStyle.gray)

          embed = discord.Embed(
            title='Light Commands',
            description='🎲 **Fun**',
            color=0x2ecc71
          )

          async def button_callback(interaction):
            embed = discord.Embed(description='Interaction Closed.')
            await interaction.response.edit_message(embed=embed, view=None)

          button.callback = button_callback

          view  = View()
          view.add_item(button)

          await interaction.response.edit_message(embed=embed, view=view)

      button.callback = button_callback
      button2.callback = button2_callback

      view  = View()
      view.add_item(button)
      view.add_item(button2)
      await ctx.reply(embed=embed, view=view)

def setup(client):
  client.add_cog(Misc(client))